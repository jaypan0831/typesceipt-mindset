import { Router } from "express";

import { createTodo } from "../controllers/todos";

const todoRoutes = Router();

todoRoutes.post("/", createTodo);

todoRoutes.get("/");

todoRoutes.patch("/:id");

todoRoutes.delete("/:id");

export default todoRoutes;
