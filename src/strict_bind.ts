const button = document.querySelector("button");

function clickHandler(message: string) {
  console.log("Result: ", message);
}

if (button) {
  // The meaning of bind(null)
  // Basically, set this to null and give the first agrument will always be 'You;re welcome'
  // https://stackoverflow.com/questions/36287018/understanding-this-method-bindnull-in-reactjs

  button.addEventListener("click", clickHandler.bind(null, "You're welcome"));
  //   button.addEventListener("click", clickHandler.bind(null)); // ERROR!!
}
