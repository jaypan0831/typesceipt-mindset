// // Default enum start from 0
// enum Role {
//   ADMIN,
//   AUTHOR,
// }

// Change starting number, or to string
enum Role {
  ADMIN = "ADMIN",
  AUTHOR = 6,
}

const person = {
  name: "Jay",
  age: 25,
  hobbies: ["skateBoarding", "drinking"],
  role: Role.ADMIN,
};

if (person.role === 6) {
  console.log("you are author");
}

if (person.role === Role.ADMIN) {
  console.log("you are admin");
}
