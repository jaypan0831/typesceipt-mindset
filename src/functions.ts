function add(n1: number, n2: number): number {
  return n1 + n2;
}

function printResult(num: number): void {
  console.log("Result: " + num);
}

function addHandle(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}

printResult(add(12, 9));

// function type
// let combineValues: function;

// specify function
let combineValues: (a: number, b: number) => number;

combineValues = add;
// combineValues = printResult; // ERROR!!

// callback
addHandle(10, 12, (result) => {
  console.log(result);
});
