// union type
type Combinable = number | string; // type ailas
type ConversionDescriptor = "as-num" | "as-text";

function combine(
  input1: Combinable,
  input2: Combinable,
  resultConversion: ConversionDescriptor
) {
  let result: Combinable;

  if (
    (typeof input1 === "number" && typeof input2 === "number") ||
    resultConversion === "as-num"
  ) {
    result = +input1 + +input2;
  } else {
    result = String(input1) + " " + String(input2);
  }

  return result;
}

const combineAges = combine(30, 26, "as-num");
console.log(combineAges);

const combineStringAges = combine("30", "26", "as-num");
console.log(combineStringAges);

const combineName = combine("Jay", "Pan", "as-text");
console.log(combineName);
