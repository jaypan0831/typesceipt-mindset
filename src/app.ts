class Department {
  protected employees: string[] = [];

  constructor(private readonly name: string) {
    this.name = name;
  }

  describe(this: Department) {
    console.log("Department: " + this.name);
  }

  addEmployee(employee: string) {
    // validation etc
    this.employees.push(employee);
  }

  printEmployeeInformation() {
    console.log(this.employees.length);
    console.log(this.employees);
  }
}

class ITDepartment extends Department {
  admins: string[];
  private reports: string[];
  private lastReport: string;

  get mostRecentReport() {
    if (this.lastReport) {
      return this.lastReport;
    }
    throw new Error("No report found!");
  }

  set mostRecentReport(value: string) {
    if (!value) {
      throw new Error("Please pass in a valid value!");
    }
    this.addReport(value);
  }

  constructor(name: string, admin: string[], reports: string[]) {
    super(name);
    this.admins = admin;
    this.reports = reports;
    this.lastReport = reports[0];
  }

  addEmployee(name: string) {
    if (name === "Manu") return;

    this.employees.push(name);
  }

  addReport(text: string) {
    this.reports.push(text);
    this.lastReport = text;
  }
}

const it = new ITDepartment("IT", ["Max"], ["something went wrong!"]);
it.mostRecentReport = "WTF!!!";
console.log(it.mostRecentReport);

it.addEmployee("Max");
it.addEmployee("Manu");
it.describe();
it.printEmployeeInformation();
