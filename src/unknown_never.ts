let userInput: unknown; // unknown is better than any
let userName: string;

userInput = 5;
userInput = "Hi";

if (typeof userInput === "string") {
  userName = userInput;
}

// never type
// the different between void and never is that void can return something but never can't
function generateError(message: string, code: number): never {
  // throw crash the function and not return anything
  throw { message, errorCode: code };
}

generateError("An error occured", 500);
